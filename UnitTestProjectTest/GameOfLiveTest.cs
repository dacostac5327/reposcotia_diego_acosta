using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScotiaProgrammingTest;
namespace UnitTestProjectTest
{
    [TestClass]
    public class GameOfLiveTest
    {
        [TestMethod]
        public void Test1GameOfLive()
        {
            GameOfLive gameOfLive = new GameOfLive();
            gameOfLive.board = gameOfLive.PaintBoard(4, 8);
            gameOfLive.board = gameOfLive.Setcells(1, 4);
            gameOfLive.board = gameOfLive.Setcells(2, 3);
            gameOfLive.board = gameOfLive.Setcells(2, 4);
            gameOfLive.board = gameOfLive.Setcells(3, 5);
            gameOfLive.GameOfLiveMain();
        }
        [TestMethod]
        public void Test2GameOfLive()
        {
            GameOfLive gameOfLive = new GameOfLive();
            gameOfLive.board = gameOfLive.PaintBoard(5, 8);
            gameOfLive.board = gameOfLive.Setcells(1, 3);
            gameOfLive.board = gameOfLive.Setcells(1, 4);
            gameOfLive.board = gameOfLive.Setcells(2, 1);
            gameOfLive.board = gameOfLive.Setcells(2, 2);
            gameOfLive.board = gameOfLive.Setcells(2, 3);
            gameOfLive.board = gameOfLive.Setcells(2, 4);
            gameOfLive.board = gameOfLive.Setcells(2, 5);
            gameOfLive.GameOfLiveMain();
        }
    }
}
