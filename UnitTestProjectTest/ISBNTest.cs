﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScotiaProgrammingTest;

namespace UnitTestProjectTest
{
    [TestClass]

    public class ISBNTest
    {
        #region validation for ISBN 10
        [TestMethod]
        public void Test_1_ISBN_10_Valid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("0471958697");
        }
        [TestMethod]
        public void Test_2_ISBN_10_Valid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("0 471 60695 2");
        }
        [TestMethod]
        public void Test_3_ISBN_10_Valid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("0-470-84525-2");
        }
        [TestMethod]
        public void Test_4_ISBN_10_Valid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("0-470-84525-2");
        }
        [TestMethod]
        public void Test_1_ISBN_10_Invalid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("0471958692");
        }
        [TestMethod]
        public void Test_2_ISBN10_Invalid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("0 471 60695 6");
        }
        [TestMethod]
        public void Test_3_ISBN_10_Invalid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("0-470-84525-4");
        }
        [TestMethod]
        public void Test_4_ISBN_10_Invalid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("0-321-14653-3");
        }
        #endregion

        #region validation for ISBN 13
        [TestMethod]
        public void Test_1_ISBN_13_Valid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("9780470059029");
        }
        [TestMethod]
        public void Test_1_ISBN_13_Invalid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("9780470059027");
        }
        [TestMethod]
        public void Test_2_ISBN_13_Valid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("978 0 471 48648 0");
        }
        [TestMethod]
        public void Test_2_ISBN_13_Invalid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("978 0 471 48648 2");
        }
        [TestMethod]
        public void Test_3_ISBN_13_Valid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("978-0596809485");
        }
        [TestMethod]
        public void Test_3_ISBN_13_Invalid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("978-0596229485");
        }
        [TestMethod]
        public void Test_4_ISBN_13_Valid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("978-0-13-149505-0");
        }
        [TestMethod]
        public void Test_4_ISBN_13_Invalid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("978-0-13-149505-5");
        }
        [TestMethod]
        public void Test_5_ISBN_13_Valid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("978-0-262-13472-9");
        }
        [TestMethod]
        public void Test_5_ISBN_13_Invalid()
        {
            ISBN iSBN = new ISBN();
            iSBN.IsbnMain("978-0-262-13472-7");
        }
        #endregion
    }
}
