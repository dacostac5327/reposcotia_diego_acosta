﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScotiaProgrammingTest
{
    public class GameOfLive
    {
        public string[,] board;
        public string[,] boardAux;

        public void GameOfLiveMain()
        {
           
            //board = gameOfLive.PaintBoard(4, 8);
            //board = gameOfLive.Setcells(1, 4);
            //board = gameOfLive.Setcells(2, 3);
            //board = gameOfLive.Setcells(2, 4);
            //board = gameOfLive.Setcells(3, 5);

            //board = PaintBoard(5, 8);
            //board = Setcells(1, 3);
            //board = Setcells(1, 4);
            //board = Setcells(2, 1);
            //board = Setcells(2, 2);
            //board = Setcells(2, 3);
            //board = Setcells(2, 4);
            //board = Setcells(2, 5);



            for (int row = 0; row < board.GetLength(0); row++)
            {
                Console.Write("\n");
                for (int column = 0; column < board.GetLength(1); column++)
                {
                    Console.Write(board[row, column] + " ");
                }
            }
            Console.Write("\n");
            Console.Write("\n");
            Console.Write("---------------------------------");
            Console.Write("\n");

            boardAux = NextGeneration();
            for (int row = 0; row < boardAux.GetLength(0); row++)
            {
                Console.Write("\n");
                for (int column = 0; column < boardAux.GetLength(1); column++)
                {
                    Console.Write(boardAux[row, column] + " ");
                }
            }
        }
        public string[,] PaintBoard(int sizerow, int sizecolumn)
        {
            board = new string[sizerow, sizecolumn];
            boardAux = new string[sizerow, sizecolumn];
            for (int row = 0; row < board.GetLength(0); row++)
            {
                for (int column = 0; column < board.GetLength(1); column++)
                {
                    board[row, column] = ".";
                    boardAux[row, column] = ".";
                }
            }
            return board;

        }

        public string[,] Setcells(int positionrow, int positioncolumn)
        {
            board[positionrow, positioncolumn] = "*";
            return board;
        }
        public string[,] NextGeneration()
        {
            for (int row = 0; row < board.GetLength(0); row++)
            {
                for (int column = 0; column < board.GetLength(1); column++)
                {
                    if (board[row, column] == "*")
                    {
                        CaptureNeighbor(row, column, true);
                    }
                    else
                    {
                        CaptureNeighbor(row, column, false);
                    }
                }
            }
            return boardAux;
        }
        public void CaptureNeighbor(int positionrow, int positioncolumn, bool isLive)
        {
            int[] rowVector = new int[] { positionrow + 1, positionrow, positionrow - 1 };
            int[] columnVector = new int[] { positioncolumn + 1, positioncolumn, positioncolumn - 1 };


            List<string> neighborsPositions = new List<string>();
            foreach (var row in rowVector)
            {
                foreach (var column in columnVector)
                {
                    if (row == positionrow && column == positioncolumn)
                    {
                        continue;
                    }
                    neighborsPositions.Add(string.Format("{0},{1}", row, column));

                }
            }
            killOrLive(neighborsPositions, string.Format("{0},{1}", positionrow, positioncolumn), isLive);
        }
        public void killOrLive(List<string> neighborsPositions, string currentPosition, bool isLive)
        {
            int liveCell = 0;
            int deadCell = 0;
            foreach (var item in neighborsPositions)
            {
                string[] values = item.Split(',');
                if (values[0] != "-1" && values[1] != "-1")
                {
                    var sizeRows = board.GetLength(0) - 1;
                    var sizeColumn = board.GetLength(1) - 1;
                    if (Convert.ToInt32(values[0]) > sizeRows || Convert.ToInt32(values[1]) > sizeColumn)
                    {
                        continue;
                    }
                    if (board[Convert.ToInt32(values[0]), Convert.ToInt32(values[1])] == "*")
                    {
                        liveCell++;
                    }
                    else
                    {
                        deadCell++;
                    }
                }
            }
            bool liveOrKillCell;
            if (isLive)
            {
                if (liveCell >= 2 && liveCell <= 3)
                {
                    liveOrKillCell = true;
                }
                else if (liveCell > 3)
                {
                    liveOrKillCell = false;
                }
                else
                {
                    liveOrKillCell = false;
                }

            }
            else
            {
                if (liveCell == 3)
                {
                    liveOrKillCell = true;
                }
                else
                {
                    liveOrKillCell = false;

                }
            }

            if (liveOrKillCell)
            {
                string[] values = currentPosition.Split(',');
                boardAux[Convert.ToInt32(values[0]), Convert.ToInt32(values[1])] = "*";
            }
        }


    }
}
