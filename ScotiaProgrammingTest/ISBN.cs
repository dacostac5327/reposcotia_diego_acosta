﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScotiaProgrammingTest
{
    public class ISBN
    {
        public static int[] arrayCompareISBN10 = new int[] { 11, 22, 33, 44, 55, 66, 77, 88, 99, 110, 121, 132, 143, 154, 165, 176, 187, 198, 209, 220, 231, 242, 253, 264, 275, 286, 297, 308, 319, 330, 341, 352, 363, 374, 385, 396, 407, 418, 429, 440, 451, 462, 473, 484, 495 };
        public static int[] arrayCompareISBN13 = new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220 };

        public void IsbnMain(string valueInput)
        {
            string codeVerificationCurrent = valueInput.Substring(valueInput.Length - 1, 1);
            valueInput = removecharter(valueInput);
            int codeVerificationValid;
            string isthatTypeISBN;
            if (valueInput.Length == 10)
            {
                codeVerificationValid = seachCodeVerification(opertaionInitial(valueInput, "_isbn10"), "_isbn10");
                isthatTypeISBN = "ISBN10";
            }
            else if (valueInput.Length == 13)
            {
                codeVerificationValid = seachCodeVerification(opertaionInitial(valueInput, "_isbn13"), "_isbn13");
                isthatTypeISBN = "ISBN13";
            }
            else
            {
                Console.WriteLine(" ISBN Invalid !");
                return;
            }

            if (Convert.ToInt32(codeVerificationCurrent) != codeVerificationValid)
            {
                Console.WriteLine(string.Format("{0} verification number invalid, your verification number valid is {1}", isthatTypeISBN, codeVerificationValid));
            }
            else
            {
                Console.WriteLine(string.Format("{0} Valid !", isthatTypeISBN));
            }
        }
        public int seachCodeVerification(int value, string isbnType)
        {
            int codeVerification = 0;
            int[] arrayCompare = null;
            if (isbnType == "_isbn10")
            {
                arrayCompare = arrayCompareISBN10;
            }
            else if (isbnType == "_isbn13")
            {
                arrayCompare = arrayCompareISBN13;
            }
            foreach (var item in arrayCompare)
            {
                if (item >= value)
                {
                    codeVerification = item - value;
                    break;
                }
            }
            return codeVerification;
        }
        public int opertaionInitial(string value, string isbnType)
        {

            int multiplier = 10;
            int normalizedvalue = 0;
            bool isValue = true;

            for (int i = 0; i < value.Length - 1; i++)
            {
                if (isbnType == "_isbn10")
                {
                    normalizedvalue += multiplier * Convert.ToInt32(value[i].ToString());
                    multiplier--;
                }
                else if (isbnType == "_isbn13")
                {
                    if (isValue)
                    {
                        normalizedvalue += Convert.ToInt32(value[i].ToString()) * 1;
                        isValue = false;
                    }
                    else
                    {
                        normalizedvalue += Convert.ToInt32(value[i].ToString()) * 3;
                        isValue = true;
                    }
                }
            }
            return normalizedvalue;
        }
        public string removecharter(string value)
        {
            value = value.Replace(" ", "");
            value = value.Replace("-", "");

            return value;
        }


    }
}
